<!DOCTYPE html>
<html>
    <head>
        <meta charset='utf-8'>
        <meta name='viewport' content='width=device-width, initial-scale=1'>
        <title>二次元配列</title>
    </head>
    <body>
    <table border="1">
    <h1>二次元配列</h1>
<?php
    $game01 = [
        "タイトル" => "ドラゴンクエストⅤ",
        "ジャンル" => "RPG",
        "ハード" => "PS2,DS,スマホ",
        "from" => "スクウェア・エニックス",
        "year" => "1992",
    ];
    $game02 =  [
        "タイトル" => "TALES OF THE ABYSS",
        "ジャンル" => "RPG",
        "ハード" => "PS2,3DS",
        "from" => "ナムコ",
        "year" => "2005",
    ];
    $game03 =  [
        "タイトル" => "原神",
        "ジャンル" => "RPG",
        "ハード" => "PC,スマホ,PS4,5",
        "from" => "Mihoyo",
        "year" => "2020",
    ];
    $games = [$game01,$game02,$game03];
    echo "<td>"."タイトル"."</td>".
        "<td>"."ジャンル"."</td>".
        "<td>"."ハード"."</td>".
        "<td>"."from"."</td>".
        "<td>"."year"."</td>";
    foreach($games as $each){
    echo "<tr>";
        echo
            "<td>".$each['タイトル']."</td>".
            "<td>".$each['ジャンル']."</td>".
            "<td>".$each['ハード']."</td>".
            "<td>".$each['from']."</td>".
            "<td>".$each['year']."</td>";
    echo "</tr>";
        }
?>
    </table>
    </body>
</html>
